var http = require("http");
var fs = require("fs");

//Metodo que genera codigos para las compras mayores a 100 mil pesos.
var codigo = Math.floor(Math.random() * (10000 - 1000)) + 1000;
//Metodo que genera un identificador para cada venta.
var id = Math.floor(Math.random() * (99999 - 999)) + 999;
//Arreglos para rescatar el valor de la compra.
var arreglo_parametros = [];
var valor = 0;

function servidor(solicitud,response) {
    
    //Metodo para reenderizar HTML desde el servidor de Node.
    response.writeHead(200, {'Content-Type':'text/html'});
    fs.readFile('./index.html', null , function(error,data) {
        if(error){
            response.writeHead(404);
            response.write('Ups paso algo.');
            
        } else{
            response.write(data);
            
        };
        response.end();
        
    });
    
    //Metodo que rescata el valor final de la compra a travez de peticion GET y genera los reportes de acuerdo a la cantidad comprada por el
    //cliente.
    
    if(solicitud.url.indexOf("?")>0){
        
        var datos_url = solicitud.url.split("?");
        arreglo_parametros = datos_url[1].split("+=");
        valor = arreglo_parametros[1];
        
        let reporte;

        //Reporte que se guardara en la base de datos.
        //Como aun no existe una base de datos, se realizo el reporte en consola.
        if(valor>100000){
            reporte = `Se realizo una compra con el ID: ${id} por el valor de: ${valor}. Por su gran compra se regala un bono de 50000 consumible con el siguiente codigo:${codigo}` ;
        }else {
            reporte = `Se realizo una compra por el valor de ${valor}. Gracias por su compra.`
        }
        
        console.log(reporte);
        
    }
    
    
}


http.createServer(servidor).listen(3000);